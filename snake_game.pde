
int w = 20;
int h = 20;
int snakeLength = 2;
int snakeHeadX;
int snakeHeadY;
char snakeDirection = 'R';
int maxSnakeLength = 500;
int[] x = new int[maxSnakeLength];
int[] y = new int[maxSnakeLength];

boolean displayTarget = true;
int targetX;
int targetY;
int backgroundColor = 80;
boolean gameOverKey = false;

void setup() {
  size(640, 640);
  frameRate(10);
  noStroke();
}

void draw() {
   if(isGameOver()) {
        snakeInit(); 
   }
  
  if(isSnakeDie()){
    showGameOver();
    return;
  }
  
  background(backgroundColor);
  // https://processing.org/reference/keyCode.html
  if (keyPressed && key == CODED) {
    switch(keyCode){
      case LEFT:
        if(snakeDirection != 'R'){
          snakeDirection = 'L';
        }
        break;
      case RIGHT:
        if(snakeDirection != 'L'){
          snakeDirection = 'R';
        }
        break;
      case DOWN:
        if(snakeDirection != 'U'){
          snakeDirection = 'D';
        }
        break;
      case UP:
        if(snakeDirection != 'D'){
          snakeDirection = 'U';
        }
        break;
    }
  }
  
  switch(snakeDirection){
    case 'L':
      snakeHeadX -= w;
      break;
    case 'R':
      snakeHeadX += w;
      break;
    case 'D':
      snakeHeadY += w;
      break;
    case 'U':
      snakeHeadY -= w;
      break;
  }
  
  drawTarget(width, height);
  
  drawSnake();
  
  if( snakeHeadX == targetX && snakeHeadY == targetY ){
    snakeLength++;
    displayTarget = true;
  }
  
}

void snakeInit(){
  snakeLength = 2;
  gameOverKey = false;
  snakeHeadX = 0;
  snakeHeadY = 0;
  snakeDirection = 'R';
}

void drawSnake() {
  for (int i=snakeLength-1; i>0; i--) {
    x[i] = x[i-1];
    y[i] = y[i-1];
  }
  
  y[0] = snakeHeadY;
  x[0] = snakeHeadX;
  
  fill(#7B6DB7);
  
  for (int i=0; i<snakeLength; i++) {
    rect(x[i], y[i], w, h);
  }
}

void drawTarget(int maxWidth, int maxHeight) {
  fill(#ED1818);
  
  if ( displayTarget ) {
    targetX = int( random(0, maxWidth)/w  ) * w;
    targetY = int( random(0, maxHeight)/h ) * h;
  }
  
  rect(targetX, targetY, w, h);
  displayTarget = false;
}

Boolean isGameOver(){
  return gameOverKey && keyPressed && (key == 'r' || key == 'R');
}

void showGameOver(){

  pushMatrix();
    
  gameOverKey = true;
  
  background(0);
  
  translate(width/2, height/2 - 50);
  
  fill(255);
  
  textAlign(CENTER);
  
  textSize(84);
  
  fill(120);
  textSize(18);
  text("Game over, press 'R' to restart.", 0, 50);
  
  popMatrix();
}


boolean isSnakeDie(){
  if( snakeHeadX < 0 || snakeHeadX >= width || snakeHeadY < 0 || snakeHeadY >= height){
    return true;
  }
  
  if( snakeLength > 2 ){
    for( int i=1; i<snakeLength; i++ ){
      if( snakeHeadX == x[i] && snakeHeadY == y[i] ){
        return true;
      }
    }
  }
  
  return false;
}
